<?php

namespace app\modules\api\controllers;

use app\models\Client;
use Yii;
use yii\rest\Controller;
use yii\web\Response;
use yii\filters\ContentNegotiator;

/**
 * Class ClientController
 * @package app\modules\api\controllers
 */
class ClientController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return Client|array
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Client();
        $model->status = Client::STATUS_NEW;
        $data = ['model' => $request->post()];

        if($model->load($data, 'model') && $model->save())
        {
            return $model;
        } else {
            return $model->errors;
        }
    }

}