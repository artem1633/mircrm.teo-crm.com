Создание лида<br>
api/client/create<br>
[<span style="color: red">POST</span>]<br>
-name (String) => 'Имя и фамилия'<br>
-living (String) => 'Страна и город'<br>
-email (String) => 'Email'<br>
-phone (String) => 'Телефон'<br>
-messager_whats_app (Integer) => 'WhatsApp'<br>
-messager_viber (Integer) => 'Viber'<br>
-messager_telegram (Integer) => 'Telegram'<br>
-messager_none (Integer) => 'Отсутствуют'<br>
-interest_aristocratic (Integer) => 'Аристократические титулы'<br>
-interest_services (Integer) => 'Поставки товаров и услуг'<br>
-interest_part (Integer) => 'Членство в профсоюзе'<br>
-interest_intercom (Integer) => 'Работа в интеркомах'<br>
-interest_help (Integer) => 'Помогите определиться'<br>
-comment (String) => 'Комментарий'<br>
-source (String) => 'Источник'<br>
ОТДАЕТ: Модель Client или ошибки<br>
