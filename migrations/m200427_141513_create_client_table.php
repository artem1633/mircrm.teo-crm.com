<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client`.
 */
class m200427_141513_create_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'manager_id' => $this->integer()->comment('Менеджер'),
            'name' => $this->string()->comment('Имя и фамилия'),
            'living' => $this->string()->comment('Страна и город'),
            'email' => $this->string()->comment('Email'),
            'phone' => $this->string()->comment('Телефон'),
            'status' => $this->integer()->comment('Статус'),
            'messager_whats_app' => $this->boolean()->defaultValue(false)->comment('WhatsApp'),
            'messager_viber' => $this->boolean()->defaultValue(false)->comment('Viber'),
            'messager_telegram' => $this->boolean()->defaultValue(false)->comment('Telegram'),
            'messager_none' => $this->boolean()->defaultValue(false)->comment('Отсутствуют'),
            'interest_aristocratic' => $this->boolean()->defaultValue(false)->comment('Аристократические титулы'),
            'interest_services' => $this->boolean()->defaultValue(false)->comment('Поставки товаров и услуг'),
            'interest_part' => $this->boolean()->defaultValue(false)->comment('Членство в профсоюзе'),
            'interest_intercom' => $this->boolean()->defaultValue(false)->comment('Работа в интеркомах'),
            'interest_help' => $this->boolean()->defaultValue(false)->comment('Помогите определиться'),
            'comment' => $this->text()->comment('Комментарий'),
            'source' => $this->string()->comment('Источник'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-client-manager_id',
            'client',
            'manager_id'
        );

        $this->addForeignKey(
            'fk-client-manager_id',
            'client',
            'manager_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-client-manager_id',
            'client'
        );

        $this->dropIndex(
            'idx-client-manager_id',
            'client'
        );

        $this->dropTable('client');
    }
}
