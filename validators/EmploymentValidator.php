<?php

namespace app\validators;

use app\models\employments\CarRentEmployment;
use Yii;
use yii\validators\Validator;
use app\models\employments\CarFixEmployment;
use app\models\employments\CarToEmployment;


/**
 * Class EmploymentValidator
 * @package app\validators
 * @property string $dateTimeStart
 * @property string $dateTimeEnd
 *
 * Отвечает за валидацию в графике:
 * Если диапазон планирования пересикается с другим планированием, то валидатор выдает ошибку
 * Валидатор принимает значение атрибута в формате: [dateFormat] - [dateFormat]
 */
class EmploymentValidator extends Validator
{
    private $dateTimeStart; // Начало проверяемого диапазона в формате Y-m-d
    private $dateTimeEnd; // Окончание проверяемого диапазона в формате Y-m-d

    /**
     * @inheritdoc
     * @param \app\models\employments\CarFixEmployment $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $dates = $model->$attribute;

        if($dates == null) {
            $this->addError($model, $attribute, 'Поле должно быть заполнено');
        } else {

            if(is_string($dates))
            {
                $dates = explode(' - ', $dates);
            }

            $this->dateTimeStart = \DateTime::createFromFormat(Yii::$app->params['dateFormat'].' H:i', $dates[0])->add(\DateInterval::createFromDateString('1 day'))->format('Y-m-d H:i');
            $this->dateTimeEnd = \DateTime::createFromFormat(Yii::$app->params['dateFormat'].' H:i', $dates[1])->sub(\DateInterval::createFromDateString('1 day'))->format('Y-m-d H:i');

            // Получаем график ремонтов авто
            $fixEmpModelId = ($model->id != null && $model instanceof CarFixEmployment) ? $model->id : -1;
            $fixEmps = CarFixEmployment::findBySql('SELECT * FROM `car_fix_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
              ( datetime_start between :start and :end or datetime_end between :start and :end ))
              AND (car_id = '.$model->car_id.') AND (id != :id)', [
                    ':start' => $this->dateTimeStart,
                    ':end' => $this->dateTimeEnd,
                    ':id' => $fixEmpModelId,
            ])->all();

            if(count($fixEmps) > 0)
                $this->addError($model, $attribute, 'В выбранный диапазон уже запланирован ремонт авто');

            $toEmpModelId = ($model->id != null && $model instanceof CarToEmployment) ? $model->id : -1;
            $toEmps = CarToEmployment::findBySql('SELECT * FROM `car_to_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
              ( datetime_start between :start and :end or datetime_end between :start and :end ))
              AND (car_id = '.$model->car_id.') AND (id != :id)', [
                ':start' => $this->dateTimeStart,
                ':end' => $this->dateTimeEnd,
                ':id' => $toEmpModelId,
            ])->all();

            if(count($toEmps) > 0)
                $this->addError($model, $attribute, 'В выбранный диапазон уже запланировано ТО авто');

            $rentEmpModelId = ($model->id != null && $model instanceof CarRentEmployment) ? $model->id : -1;
            $rentEmps = CarRentEmployment::findBySql('SELECT * FROM `car_rent_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
              ( datetime_start between :start and :end or datetime_end between :start and :end ))
              AND (car_id = '.$model->car_id.') AND (id != :id)', [
                ':start' => $this->dateTimeStart,
                ':end' => $this->dateTimeEnd,
                ':id' => $rentEmpModelId,
            ])->all();

            if(count($rentEmps) > 0)
                $this->addError($model, $attribute, 'В выбранный диапазон уже запланирована аренда авто');
        }


    }
}