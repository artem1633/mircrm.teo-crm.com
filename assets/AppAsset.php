<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'libs/flatpickr-master/dist/flatpickr.min.css',
        'https://vjs.zencdn.net/7.6.5/video-js.css'
    ];
    public $js = [
        'libs/flatpickr-master/dist/flatpickr.min.js',
        'libs/flatpickr-master/dist/I10n/ru.js',
        'libs/cleave/cleave.min.js',
        'https://vjs.zencdn.net/7.6.5/video.js',
        'js/common.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
