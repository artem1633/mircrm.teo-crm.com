<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client;

/**
 * ClientSearch represents the model behind the search form about `app\models\Client`.
 */
class ClientSearch extends Client
{
    public $messagers;
    public $interesting;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'manager_id', 'status', 'messager_whats_app', 'messager_viber', 'messager_telegram', 'messager_none', 'interest_aristocratic', 'interest_services', 'interest_part', 'interest_intercom', 'interest_help'], 'integer'],
            [['name', 'living', 'email', 'phone', 'comment', 'source', 'created_at', 'messagers', 'interesting'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'manager_id' => $this->manager_id,
            'status' => $this->status,
            'messager_whats_app' => $this->messager_whats_app,
            'messager_viber' => $this->messager_viber,
            'messager_telegram' => $this->messager_telegram,
            'messager_none' => $this->messager_none,
            'interest_aristocratic' => $this->interest_aristocratic,
            'interest_services' => $this->interest_services,
            'interest_part' => $this->interest_part,
            'interest_intercom' => $this->interest_intercom,
            'interest_help' => $this->interest_help,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'living', $this->living])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'source', $this->source]);

        if($this->messagers != null){
            if(in_array('messager_whats_app', $this->messagers)){
                $query->orWhere(['messager_whats_app' => 1]);
            }
            if(in_array('messager_viber', $this->messagers)){
                $query->orWhere(['messager_viber' => 1]);
            }
            if(in_array('messager_telegram', $this->messagers)){
                $query->orWhere(['messager_telegram' => 1]);
            }
            if(in_array('messager_none', $this->messagers)){
                $query->orWhere(['messager_none' => 1]);
            }
        }

        if($this->interesting != null){
            if(in_array('interest_aristocratic', $this->interesting)){
                $query->orWhere(['interest_aristocratic' => 1]);
            }
            if(in_array('interest_services', $this->interesting)){
                $query->orWhere(['interest_services' => 1]);
            }
            if(in_array('interest_part', $this->interesting)){
                $query->orWhere(['interest_part' => 1]);
            }
            if(in_array('interest_intercom', $this->interesting)){
                $query->orWhere(['interest_intercom' => 1]);
            }
            if(in_array('interest_help', $this->interesting)){
                $query->orWhere(['interest_help' => 1]);
            }
        }

        if($this->created_at != null){
            $date = explode(' - ', $this->created_at);
            $query->andWhere(['between', 'created_at', $date[0].' 00:00:00', $date[1].' 23:59:59']);
        }

        return $dataProvider;
    }
}
