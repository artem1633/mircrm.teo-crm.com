<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property int $manager_id Менеджер
 * @property string $name Имя и фамилия
 * @property string $living Страна и город
 * @property string $email Email
 * @property string $phone Телефон
 * @property int $status Статус
 * @property int $messager_whats_app WhatsApp
 * @property int $messager_viber Viber
 * @property int $messager_telegram Telegram
 * @property int $messager_none Отсутствуют
 * @property int $interest_aristocratic Аристократические титулы
 * @property int $interest_services Поставки товаров и услуг
 * @property int $interest_part Членство в профсоюзе
 * @property int $interest_intercom Работа в интеркомах
 * @property int $interest_help Помогите определиться
 * @property string $comment Комментарий
 * @property string $source Источник
 * @property string $created_at
 *
 * @property User $manager
 */
class Client extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_WORKS = 2;
    const STATUS_OFFERS = 3;
    const STATUS_SALES = 4;
    const STATUS_NOCONNECT = 5;
    const STATUS_WAITING = 6;
    const STATUS_CANCEL = 7;
    const STATUS_NOINTEREST = 8;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['manager_id', 'status', 'messager_whats_app', 'messager_viber', 'messager_telegram', 'messager_none', 'interest_aristocratic', 'interest_services', 'interest_part', 'interest_intercom', 'interest_help'], 'integer'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
            [['name', 'living', 'email', 'phone', 'source'], 'string', 'max' => 255],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['manager_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manager_id' => 'Менеджер',
            'name' => 'Имя и фамилия',
            'living' => 'Страна и город',
            'email' => 'Email',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'messager_whats_app' => 'WhatsApp',
            'messager_viber' => 'Viber',
            'messager_telegram' => 'Telegram',
            'messager_none' => 'Отсутствуют',
            'interest_aristocratic' => 'Аристократические титулы',
            'interest_services' => 'Поставки товаров и услуг',
            'interest_part' => 'Членство в профсоюзе',
            'interest_intercom' => 'Работа в интеркомах',
            'interest_help' => 'Помогите определиться',
            'comment' => 'Комментарий',
            'source' => 'Источник',
            'created_at' => 'Дата и время',
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
       return [
           self::STATUS_NEW => 'Новый',
           self::STATUS_WORKS => 'В работе',
           self::STATUS_OFFERS => 'Офферы',
           self::STATUS_SALES => 'Покупки',
           self::STATUS_NOCONNECT => 'Нет связи',
           self::STATUS_WAITING => 'Думает',
           self::STATUS_NOINTEREST => 'Без интереса',
       ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }
}
