<?php

use yii\helpers\Url;
use app\models\Client;

$isClientController = Yii::$app->controller->id == 'client';
$action = Yii::$app->controller->action;

$newCount = 0;
$workCount = 0;
$offersCount = 0;
$salesCount = 0;
$noconnectCount = 0;
$waitingCount = 0;
$cancelCount = 0;
$nointerestCount = 0;
$allCount = 0;

if(Yii::$app->user->identity->isSuperAdmin()){
    $newCount = Client::find()->where(['status' => Client::STATUS_NEW])->count();
    $workCount = Client::find()->where(['status' => Client::STATUS_WORKS])->count();
    $offersCount = Client::find()->where(['status' => Client::STATUS_OFFERS])->count();
    $salesCount = Client::find()->where(['status' => Client::STATUS_SALES])->count();
    $noconnectCount = Client::find()->where(['status' => Client::STATUS_NOCONNECT])->count();
    $waitingCount = Client::find()->where(['status' => Client::STATUS_WAITING])->count();
    $cancelCount = Client::find()->where(['status' => Client::STATUS_CANCEL])->count();
    $nointerestCount = Client::find()->where(['status' => Client::STATUS_NOINTEREST])->count();
    $allCount = Client::find()->count();
} else {
    $newCount = Client::find()->where(['status' => Client::STATUS_NEW])->count();
    $workCount = Client::find()->where(['status' => Client::STATUS_WORKS, 'manager_id' => Yii::$app->user->getId()])->count();
    $offersCount = Client::find()->where(['status' => Client::STATUS_OFFERS, 'manager_id' => Yii::$app->user->getId()])->count();
    $salesCount = Client::find()->where(['status' => Client::STATUS_SALES, 'manager_id' => Yii::$app->user->getId()])->count();
    $noconnectCount = Client::find()->where(['status' => Client::STATUS_NOCONNECT, 'manager_id' => Yii::$app->user->getId()])->count();
    $waitingCount = Client::find()->where(['status' => Client::STATUS_WAITING, 'manager_id' => Yii::$app->user->getId()])->count();
    $cancelCount = Client::find()->where(['status' => Client::STATUS_CANCEL, 'manager_id' => Yii::$app->user->getId()])->count();
    $nointerestCount = Client::find()->where(['status' => Client::STATUS_NOINTEREST, 'manager_id' => Yii::$app->user->getId()])->count();
    $allCount = Client::find()->where(['manager_id' => Yii::$app->user->getId()])->count();
}


?>

<?php \yii\widgets\Pjax::begin(['id' => 'menu-pjax-container']) ?>
    <div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Пользователи','encode' => false, 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Новые <span class="label label-warning" style="font-size: 11px;">'.$newCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client'], 'active' => ($isClientController && $action->id == 'index'), 'linkOptions' => ['data-pjax' =>'0']],
                    ['label' => 'В работе <span class="label label-warning" style="font-size: 11px;">'.$workCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client-work'], 'active' => ($isClientController && $action->id == 'work'), 'linkOptions' => ['data-pjax' =>'0']],
                    ['label' => 'Офферы <span class="label label-warning" style="font-size: 11px;">'.$offersCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client-offers'], 'active' => ($isClientController && $action->id == 'offers'), 'linkOptions' => ['data-pjax' =>'0']],
                    ['label' => 'Покупки <span class="label label-warning" style="font-size: 11px;">'.$salesCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client-sales'], 'active' => ($isClientController && $action->id == 'sales'), 'linkOptions' => ['data-pjax' =>'0']],
                    ['label' => 'Нет связи <span class="label label-warning" style="font-size: 11px;">'.$noconnectCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client-noconnect'], 'active' => ($isClientController && $action->id == 'noconnect'), 'linkOptions' => ['data-pjax' =>'0']],
                    ['label' => 'Думают <span class="label label-warning" style="font-size: 11px;">'.$waitingCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client-waiting'], 'active' => ($isClientController && $action->id == 'waiting'), 'linkOptions' => ['data-pjax' =>'0']],
                    ['label' => 'Отказы <span class="label label-warning" style="font-size: 11px;">'.$cancelCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client-cancel'], 'active' => ($isClientController && $action->id == 'cancel'), 'linkOptions' => ['data-pjax' =>'0']],
                    ['label' => 'Без интереса <span class="label label-warning" style="font-size: 11px;">'.$nointerestCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client-nointerest'], 'active' => ($isClientController && $action->id == 'nointerest'), 'linkOptions' => ['data-pjax' =>'0']],
                    ['label' => 'Все <span class="label label-warning" style="font-size: 11px;">'.$allCount.'</span>','encode' => false, 'icon' => 'fa  fa-folder-open', 'url' => ['/client-all'], 'active' => ($isClientController && $action->id == 'all'), 'linkOptions' => ['data-pjax' =>'0']],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
<?php \yii\widgets\Pjax::end() ?>