<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
?>
<div class="client-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'manager_id',
            'name',
            'living',
            'email:email',
            'phone',
            'status',
            'messager_whats_app',
            'messager_viber',
            'messager_telegram',
            'messager_none',
            'interest_aristocratic',
            'interest_services',
            'interest_part',
            'interest_intercom',
            'interest_help',
            'comment:ntext',
            'source',
            'created_at',
        ],
    ]) ?>

</div>
