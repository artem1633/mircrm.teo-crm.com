<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Лиды";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$panelBeforeTemplate = '';

if(Yii::$app->user->identity->isSuperAdmin() && Yii::$app->controller->action->id == 'index'){
    $panelBeforeTemplate .= Html::a('Добавить <i class="fa fa-plus"></i>', ['client/create'],
            ['role'=>'modal-remote','title'=> 'Добавить лид','class'=>'btn btn-success']).'&nbsp;';
}

$panelBeforeTemplate .= Html::a('<i class="fa fa-repeat"></i>', [''],
    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']);

?>
<div class="panel panel-inverse client-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Лиды</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            "pjaxSettings" => [
                'options' => [
                    'timeout' => 5000,
                ],
            ],
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' => $panelBeforeTemplate,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>BulkButtonWidget::widget([
            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
            ["client/bulk-delete"] ,
            [
            "class"=>"btn btn-danger btn-xs",
            'role'=>'modal-remote-bulk',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
            ]),
            ]).
            '<div class="clearfix"></div>',
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
