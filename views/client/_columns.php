<?php
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Client;
use app\models\User;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'manager_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'value' => 'manager.name',
        'filter' => ArrayHelper::map(User::find()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'living',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'messagers',
        'label' => 'Мессенджеры',
        'content' => function($model){
            $output = '';

            if($model->messager_whats_app == 1){
                $output .= '<i class="fa fa-whatsapp text-success" style="font-size: 18px;"></i> ';
            }

            if($model->messager_viber == 1){
                $output .= '<span class="label label-warning label-sm" style="font-size: 11px;">Viber</span> ';
            }

            if($model->messager_telegram == 1){
                $output .= '<i class="fa fa-telegram text-info" style="font-size: 18px;"></i> ';
            }

            if($model->messager_none == 1){
                $output .= '<i class="fa fa-times text-danger" style="font-size: 18px;"></i>';
            }

            return $output;
        },
        'filter' => [
            'messager_whats_app' => 'WhatsApp',
            'messager_viber' => 'Viber',
            'messager_telegram' => 'Telegram',
            'messager_none' => 'Отсутсвует',
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'interesting',
        'label' => 'Заинтересовало',
        'content' => function($model){
            $attrs = ['interest_aristocratic', 'interest_services', 'interest_part', 'interest_intercom', 'interest_help'];
            $arr = [];

            foreach ($attrs as $attr){
                if($model->$attr == 1){
                    $arr[] = $model->getAttributeLabel($attr);
                }
            }

            $output = implode(', ', $arr);

            return $output;
        },
        'filter' => [
            'interest_aristocratic' => 'Аристократические титулы',
            'interest_services' => 'Поставки товаров и услуг',
            'interest_part' => 'Членство в профсоюзе',
            'interest_intercom' => 'Работа в интеркомах',
            'interest_help' => 'Помогите определиться',
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'value' => function($model){
            return ArrayHelper::getValue(Client::statusLabels(), $model->status);
        },
        'filter' => Client::statusLabels(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'width' => '8%',
        'visible' => Yii::$app->controller->action->id == 'all',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'status',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'messager_whats_app',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'messager_viber',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'messager_telegram',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'messager_none',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'interest_aristocratic',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'interest_services',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'interest_part',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'interest_intercom',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'interest_help',
    // ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'comment',
//     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'source',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
         'filterType' => GridView::FILTER_DATE_RANGE,
         'filterWidgetOptions' => [
             'convertFormat'=>true,
             'pluginEvents' => [
                 'cancel.daterangepicker'=>'function(ev, picker) {$("#clientsearch-created_at").val(""); $("#clientsearch-created_at").trigger("change"); }'
             ],
             'pluginOptions' => [
                 'opens'=>'right',
                 'locale' => [
                     'cancelLabel' => 'Clear',
                     'format' => 'Y-m-d',
                 ]
             ]
         ],
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(['client/'.$action,'id'=>$key]);
        },
        'template' => '{take}{update}{delete}',
        'buttons' => [
            'take' => function ($url, $model) {
                if(Yii::$app->controller->action->id == 'index'){
                    return Html::a('<i class="fa fa-plus-square text-info" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Взять в работу',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите взять в работу данного лида?'
                    ])."&nbsp;";
                }
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   