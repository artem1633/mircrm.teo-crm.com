<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */

$statusLabels = \app\models\Client::statusLabels();

//if($model->isNewRecord == false){
//    unset($statusLabels[\app\models\Client::STATUS_NEW]);
//}

?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'living')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'status')->dropDownList($statusLabels) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h4>Мессенджеры</h4>
            <?= $form->field($model, 'messager_whats_app')->checkbox() ?>

            <?= $form->field($model, 'messager_viber')->checkbox() ?>

            <?= $form->field($model, 'messager_telegram')->checkbox() ?>

            <?= $form->field($model, 'messager_none')->checkbox() ?>
        </div>
        <div class="col-md-6">
            <h4>Меня заинтересовало</h4>
            <?= $form->field($model, 'interest_aristocratic')->checkbox() ?>

            <?= $form->field($model, 'interest_services')->checkbox() ?>

            <?= $form->field($model, 'interest_part')->checkbox() ?>

            <?= $form->field($model, 'interest_intercom')->checkbox() ?>

            <?= $form->field($model, 'interest_help')->checkbox() ?>
        </div>
    </div>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
