<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\User;
use app\models\Client;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
        'content' => function($model){
            $output = "<b style='font-size: 14px; margin-bottom: 18px;'>$model->login</b>";

            $workCount = 0;
            $offersCount = 0;
            $salesCount = 0;
            $noconnectCount = 0;
            $waitingCount = 0;
            $cancelCount = 0;
            $nointerestCount = 0;
            $allCount = 0;

            $workCount = Client::find()->where(['status' => Client::STATUS_WORKS, 'manager_id' => $model->id])->count();
            $offersCount = Client::find()->where(['status' => Client::STATUS_OFFERS, 'manager_id' => $model->id])->count();
            $salesCount = Client::find()->where(['status' => Client::STATUS_SALES, 'manager_id' => $model->id])->count();
            $noconnectCount = Client::find()->where(['status' => Client::STATUS_NOCONNECT, 'manager_id' => $model->id])->count();
            $waitingCount = Client::find()->where(['status' => Client::STATUS_WAITING, 'manager_id' => $model->id])->count();
            $cancelCount = Client::find()->where(['status' => Client::STATUS_CANCEL, 'manager_id' => $model->id])->count();
            $nointerestCount = Client::find()->where(['status' => Client::STATUS_NOINTEREST, 'manager_id' => $model->id])->count();
            $allCount = Client::find()->where(['manager_id' => $model->id])->count();

            $output .= "<p>
                В работе: <b>{$workCount}</b><br>
                Офферы: <b>{$offersCount}</b><br>            
                Покупки: <b>{$salesCount}</b><br>            
                Нет связи: <b>{$noconnectCount}</b><br>            
                Думают: <b>{$waitingCount}</b><br>            
                Отказы: <b>{$cancelCount}</b><br>            
                Без интереса: <b>{$nointerestCount}</b><br>            
                Всего: <b>{$allCount}</b>   
            </p>";

            return $output;
        },
        'width' => '400px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'role',
        'value' => function($model){
            if(isset(User::roleLabels()[$model->role])){
                return User::roleLabels()[$model->role];
            }
        },
        'vAlign' => \kartik\grid\GridView::ALIGN_MIDDLE
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'vAlign' => \kartik\grid\GridView::ALIGN_MIDDLE
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'is_deletable',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update} {delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данного пользователя?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ]);
            },
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Сохранить', 'data-toggle'=>'tooltip'],
    ],

];   